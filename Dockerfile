FROM prom/prometheus 
ADD prometheus.yml /etc/prometheus/

CMD        [ "-config.file=/etc/prometheus/prometheus.yml", \
             "-storage.local.path=/prometheus", \
             "-web.console.libraries=/usr/share/prometheus/console_libraries", \
             "-web.console.templates=/usr/share/prometheus/consoles", \
             "-alertmanager.url=https://alertmanager-cw-portal-plg.playground.itandtel.at" ]